# Simple script to run one XML application

$f = shift @ARGV;
$w = shift @ARGV;

open F, "<$f" or die("Couldn't open $f: $!");

$s = join '', <F>;
close F;
print "DO: $s\n";

require 'XGtk.pm';

$x = XGtk->new($s);

$wid = $x->create($w);

print "ENTERING MAIN LOOP\n";
Gtk->main;


