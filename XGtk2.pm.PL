# XGtk - The Gtk XML programming aid.
# Copyright (C) 1998-1999 Tuomas J. Lukka
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free
# Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#

#-------------------

# Ok, so now we've done it! XGtk is now almost completely written in XML
# itself. This makes it easier to comment the elements and attributes
# and keep the elements up to date. The organization is much
# clearer, also. Unfortunately we now have to generate Perl from the XML in 
# the end of this file first. To save time, we always do this at compile time.
#
# If you have any trouble understanding what is going on, please
# email me at lukka@iki.fi, I'll be glad to help.
#

use XML::Parser;
use XML::Grove;
use XML::Parser::Grove;

my $parser = XML::Parser->new(Style => 'grove');
my $grove = $parser->parse(\*DATA);

use Data::Dumper;
$Data::Dumper::Indent = 1;

my ($prog) = @{$grove->{contents}};
print Dumper($prog);

my $pc = $prog->{contents};

my $p = "";

my @hdr = grep { ref $_ and $_->{name} eq "Hdr" } @$pc;
my @tail = grep { ref $_ and $_->{name} eq "Tail" } @$pc;
my @things = grep { ref $_ and $_{name !~ /^Hdr|Tail$/ } @$pc;

for(@hdr) {
	$p .= join '', @{$_->contents};
}

# Now, the difficult part: the actual generation.

$p .= 'my %elsubs = (';

for my $t (@things) {
	$p .= " $t->{attributes}{name} => { Type => $t->{name}, \n" ;
	my $defs = $t->{attributes}{defsignal};
	my @c = grep {ref $_} @{$t->{contents}};
	my @attr = map {$_->{attributes}} grep {$_->{name} eq "Attr"} @c;
	my ($create) = grep {$_->{name} eq "create"} @c;
	my ($apchild) = grep {$_->{name} eq "appendchild"} @c;

}

$p .= ');';


for(@tail) {
	$p .= join '', @{$_->contents};
}

__DATA__

<Program>
<!-- XGtk 2 is written in XML and a simple Perl script which unwraps it

	$__sub = the sub ref from a scriptlike thing.
	$__defsignal = default signal of parent
	$__parent = parent widget
	$__obj = the object

-->

	<Hdr>
		# THIS FILE IS AUTOMATICALLY GENERATED FROM
		# XGtk2.pm.PL - DO NOT EDIT - CHANGES WILL BE OVERWRITTEN.
		# EDIT XGtk2.pm.PL INSTEAD

		package XGtk::Widget;

		sub AUTOLOAD {
			my $this = shift;
			my $n = $AUTOLOAD;
			$n =~ s/.*:://;
			if($this->{__meth}{$n}) {
				# print "CALL METHOD $n (@_)\n";
				$this->{__meth}{$n}->(@_);
			} else {
				print "Available: ",(join ' ',keys %{$this->{__meth}}),"\n";
				die("No method '$n' found");
			}
		}

		sub xgtk_destroy {
			print "XGTK_DESTROY: '$_[0]' '$_[0]{__destr}'\n";
			for(@{$_[0]{__destr}}) {&amp;{$_}()}
			for(@{$_[0]{__widg}}) {
				print "D $_\n";
				$_->destroy if ref $_;
			}
			%{$_[0]} = ();
		}

		sub DESTROY {
			$_[0]->xgtk_destroy;
		}


		package XGtk;
		use Gnome;
		Gnome->init("Foo");

		use XML::Parser;
		use XML::Grove;
		use XML::Parser::Grove;
		use FileHandle;

		use Data::Dumper;

		sub new {
			my($type, $file) = @_;
			my $parser = XML::Parser->new(Style => 'grove');
			my $grove = $parser->parse($file);
			print $grove;
			my $this = bless {
				Grove => $grove,
				ID => $grove->get_ids("name", {CLASS => 1}),
			}, $type;
			return $this;
		}

	</Hdr>



	<Widget name="Button" defsignal="clicked">
		<Attr name="label"/>
		<create>
			if(defined $label) {
				return Gtk::Button->new_with_label($l);
			} else {
				return Gtk::Button->new();
			}
		</create>
	</Widget>

	<Widget name="VBox">
		<Attr name="homog"/>
		<Attr name="spacing"/>
		<create>
			return Gtk::VBox->new($homog, $spacing);
		</create>
	</Widget>

	<Widget name="HBox">
		<Attr name="homog"/>
		<Attr name="spacing"/>
		<create>
			return Gtk::HBox->new($homog, $spacing);
		</create>
	</Widget>

	<Widget name="Table">
		<Attr name="homog"/>
		<Attr name="x"/>
		<Attr name="y"/>
		<create>
			return Gtk::Table->new($x, $y, $homog);
		</create>
	</Widget>

	<Widget name="MenuBar">
		<Attr name="shadow"/>
		<create>
			my $m = Gtk::MenuBar->new();
			if(defined $shadow) {
				$m->set(shadow, $shadow);
			}
			return $m;
		</create>
		<appendchild>
			$w->append($c);
		</appendchild>
	</Widget>

	<Widget name="Menu">
		<create>
			return Gtk::Menu->new();
		</create>
		<appendchild>
			$w->append($c);
		</appendchild>
	</Widget>

	<Widget name="MenuItem">
		<Attr name="label"/>
		<create>
			if(defined $label) {
				return Gtk::MenuItem->new_with_label($l);
			} else {
				return Gtk::MenuItem->new();
			}
		</create>
		<appendchild>
			if(ref $c =~ /Menu/) {
				$w->set_submenu($c);
			} else {
				$w->add($c);
			}
		</appendchild>
	</Widget>

	<Widget name="RadioMenuItem">
		<Attr name="label"/>
		<Attr name="group" type="wid"/>
		<create>
			# XXX get rid of obligatory group
			if($label) {
				return Gtk::RadioMenuItem->new($label,$group);
			} else {
				return Gtk::RadioMenuItem->new(undef,$group);
			}
		</create>
	</Widget>

	<ModChild name="pack">
		<Attr name="expand"/>
		<Attr name="fill"/>
		<Attr name="padding"/>
		<appendchild>
			$__parent->pack_start($c, $expand, $fill, $padding);
		</appendchild>
	</ModChild>

	<ModChild name="tableCell">
		<Attr name="x0"/>
		<Attr name="x1"/>
		<Attr name="y0"/>
		<Attr name="y1"/>
		<appendchild>
			$__parent->attach_defaults($c, $x0, $x1, $y0, $y1);
		</appendchild>
	</ModChild>

	<!-- Tablerow -->

	<ScriptLike name="timeout">
		<Attr name="interval"/>
		<help>
			Remember that returning false from the handler
			will cause the timeout not to repeat any more.
		</help>
		<create>
			return Gtk->timeout_add($interval, $__sub);
		</create>
	</ScriptLike>

	<ScriptLike name="script">
		<Attr name="signal"/>
		<create>
			if(!defined $signal) {
				$signal = $__defsignal;
			}
			$__parent->signal_connect($signal, $__sub);
		</create>
	</ScriptLike>

	<ScriptLike name="method">
		<Attr name="name" type="str"/>
		<create>
			$__obj->{__meth}{$name} = $__sub;
		</create>
	</ScriptLike>

	<ScriptLike name="constructor">
		<create>
			$__obj->{__constr} = $__sub;
		</create>
	</ScriptLike>

	<ScriptLike name="destructor">
		<create>
			$__obj->{__destr} = $__sub;
		</create>
	</ScriptLike>


	<Tail>
		###
		### Code borrowed from Ken MacLeod's IDs.pm module
		### This will eventually be part of XML:::Grove
		###

		package XML::Grove::IDs;

		use XML::Grove::Visitor;

		sub new {
		    my ($type, $name, $elements) = @_;
		    $name = 'id' if(!defined $name);
		    return (bless {Name => $name, Elements => $elements}, $type);
		}

		sub visit_grove {
		    my $self = shift; my $grove = shift; my $hash = shift;
		    $grove->children_accept ($self, $hash);
		}

		sub visit_element {
		    my $self = shift; my $element = shift; my $hash = shift;

		    if(!$self->{Elements} or $self->{Elements}{$element->{name}}) {
			    my $id = $element->attr($self->{Name});
			    $hash->{$id} = $element
				if (defined $id);
		    }

		    $element->children_accept ($self, $hash);
		}

		sub visit_entity {
		}

		sub visit_pi {
		}

		sub visit_comment {
		}

		sub visit_scalar {
		}

		###
		### Extend the XML::Grove and XML::Grove::Element packages with our
		### new function.
		###

		package XML::Grove;

		sub get_ids {
		    my $self = shift;

		    my $hash = {};
		    $self->accept(XML::Grove::IDs->new(@_), $hash);
		    return $hash;
		}

		package XML::Grove::Element;

		sub get_ids {
		    my $self = shift;

		    my $hash = {};
		    $self->accept(XML::Grove::IDs->new(@_), $hash);
		    return $hash;
		}

		package XML::Grove::Iter;

		sub get_ids {
		    my $self = shift;

		    my $hash = {};
		    $self->accept(XML::Grove::IDs->new(@_), $hash);
		    return $hash;
		}

		package XML::Grove::Element::Iter;

		sub get_ids {
		    my $self = shift;

		    my $hash = {};
		    $self->accept(XML::Grove::IDs->new(@_), $hash);
		    return $hash;
		}

		1;

	</Tail>

</Program>



