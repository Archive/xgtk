# XGtk - The Gtk XML programming aid.
# Copyright (C) 1998 Tuomas J. Lukka
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free
# Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#


=head1 NAME

XGtk -- Generate Gtk applications from XML 

=head1 SYNOPSIS

	$s = '
		<TopLevel>
		 <Button text="Quit">
		  <script signal="clicked">
		  	Gtk->main_quit;
		  </script>
		 </Button>
		</TopLevel>
	';

	$x = XGtk->new($s);
	$o = $x->create();
	Gtk->main;

=head1 DESCRIPTION

WARNING: THIS DOCUMENTATION LAGS BEHIND THE ACTUAL MODULE A BIT -
CHECK THE SOURCE CODE FOR LATEST INFORMATION ON SUPPORTED WIDGETS.

The C<XGtk> module implements an XML document style which is used
to create Gtk widgets. The application is described in terms of
classes, each of which consists of a widget tree and (possibly)
Perl scriplets for methods and constructors.

There are three kinds of elements in the XML tree, distinguished 
by their capitalization. C<UPPERCASE> elements have to do with the 
object hierarchy. C<Capitalized> elements are Gtk widgets and 
C<lowercase> elements contain either Perl code or other, non-widget
data about the user interface.

Each Capitalized element accepts the C<id> attribute, giving a name
to that widget. The Gtk widget object can then be referred in scripts as

	$this->{name}

if it was defined with C<id="name">

=head2 Object model

The root element of the file should be C<XGtk>. 
Inside that element, there can be one C<TopLevel> (the main window)
and several C<CLASS> declarations.

Classes can inherit methods or parts of widget hierarchies
from other classes. To inherit a number of methods, just do

	<inherit from="ClassName" method="method1,method2,method3"/>

To inherit widget hierarchies, you can use a different form of the
inheritance directive:

	<inherit from="ClassName" widget="textwindow">
		<replace widget="toolframe">
			<Label text="Tools disabled"/>
		</replace>
		...
	</inherit>

=head2 Methods

The main package, C<XGtk> has two methods, used like

	$x = XGtk->new($xml_string_or_filehandle);

and 

	$o = $x->create($class_name, $param1, ...);

where $class_name may be C<undef>. The parameters of C<create>
following the class name are passed directly to the constructors 
of the class. C<create> returns an object of type C<XGtk::Widget>
which 
The original XGtk object $x is stored inside the returned object,
and can be accessed with commands like

	$this->{XGtk}->create("Subwindow", "fofoo");

inside the C<script> and other Perl code embedded in the C<CLASS>.

=head2 Element types

The currently known element types are 

=over 8

=item TopLevel

same as 

	Gtk::Window->new('toplevel')

=item VBox/HBox homog="0" spacing="3"

=item Entry

=item Text

=item MenuBar

=item Menu

=item MenuItem text="text" label="text" method="methodname"

A menu item, with text. If the attribute C<method> is given,
that method (of the XGtk::Widget object)
is connected to the C<activate> signal (default signal for this widget).
C<method> may be a comma-separated list of several methods to be called
in that order.

The same behaviour may be accomplished as

	<MenuItem text="text">
		<script signal="activate">
			$this->methodname(@_)
		</script>
	</MenuItem>

=item Button text="text" label="text" method="methodname" pixmap="filename"

Same as above, except that the default signal is ``C<clicked>''.

=item RadioButton text="text" label="text" group="grpname"

The group attribute is to be used like

	<RadioButton text="Button 1" id="but1"/>
	<RadioButton text="Button 2" group="but1"/>
	<RadioButton text="Button 3" group="but1"/>

=item Label text="text" label="text"

=item Frame label="text"

=item Table x="5" y="10"

A table. Use C<tableCell>, C<tableRow> and C<tableColumn> inside.

=item tableCell x0="2" x1="3" y0="1" y1="5"

Attach the widget inside to the table outside.

=item tableColumn x0="2" x1="3"

Attach the widgets inside to the table outside, starting from 
the top edge of the table (XXX FIXME).

=item tableRow y0="2" y1="3"

=item GnomeDialog title="Title"

This element describes a GNOME dialog. It can contain normal elements
(in its middle area) as well as special C<dialogButton> elements (see below).

=item GnomeAbout title=".." version=".." copyright=".." authors=".." comments=".."

An About box. 

=item script signal="signal_name"

A Perl scriplet to be connected to the signal C<signal_name> of
the enclosing widget. The variable $this is predefined to be the 
current object. This element must only contain character data.

If the attribute C<signal> is omitted, the I<default signal> of the 
enclosing widget (e.g. C<clicked> for a C<Button>) is used.

=item method name="method_name"

Declares a method of the name C<method_name> for the current class.
Contents as for C<script>.

=item dialogButton name="OK" method="method_name"

This element can only be used inside a C<GnomeDialog> element.
It represents a button at the lower edge of the dialog.
This element can have a C<method> attribute, which is, as usual,
a comma-separated list of methods to call if this button is clicked.
This item can also contain a scriplet to be also called when the 
button is clicked.

The button C<name> can either be a stock button name, like C<OK>
or C<CANCEL> or simply a string.

=item timeout interval="500"

Declares a scriplet to be executed every C<interval> milliseconds.
Contents of the element are the same as for C<script>.

=item constructor

Declares a constructor for the class, to be run when the class
is initialized. The parameters passed to the creation function
are passed in @_ (there is no type element in the beginning).
Note that the first argument of the C<create> function is the object
class name to be created.

=item pack expand="1" fill="0" padding="42"

Declares a new packing method. The elements I<inside> the C<pack>
tag are packed with the corresponding parameters (the default,
just inside a tag, is to use the C<add> command, except for some
special cases like menus)

=back

=head1 BUGS

Does not give access to all Gnome / Gtk capabilities YET.

=head1 AUTHOR

Copyright(C) 1998 Tuomas J. Lukka (lukka@iki.fi)

=cut

package XGtk;

# use Gtk;
use Gnome;
Gnome->init("Foo"); 

Gtk::Rc->parse("$ENV{HOME}/.gtkrc");

my $tooltips = Gtk::Tooltips->new();

use XML::Parser;
use XML::Grove;
use XML::Parser::Grove;
use FileHandle;

use Data::Dumper;

sub new {
	my($type, $file) = @_;
	my $parser = XML::Parser->new(Style => 'grove');
	my $grove = $parser->parse($file);
	print $grove;
	my $this = bless {
		Grove => $grove,
		ID => $grove->get_ids("name", {CLASS => 1}),
	}, $type;
	return $this;
}

sub create {
	my($this, $which, @args) = @_;
	my $class;
	my $grove = $this->{Grove};
	if($which) {
		$class = $this->{ID}{$which} or die "No id '$which' found";
	} else {
		my ($root) = grep {ref $_ eq "XML::Grove::Element"}
				@{$grove->{contents}}
				 or die("Root not found?!");
		if($root->name eq "TopLevel") {
			$class = $root;
		} else {
			($class)  = grep {ref $_ eq "XML::Grove::Element"
					and $_->name eq "TopLevel"} @{$root->{contents}}
					 or die("TopLevel not found");;
		}
	}
	my $obj = bless {
		XGtk => $this, 
	}, XGtk::Widget;

	if($class->name eq "CLASS") {
		for(@{$class->{contents}}) {
			next unless ref $_;
			push @{$obj->{__widg}}, $this->create_widget(
				$obj, $_, undef, sub {}
			);
		}
	} else {
		$obj->{__widg} = [$this->create_widget(
			$obj, $class, undef, sub {}
		)];
	}
	for(@{$obj->{__constr}}) {
		my $this = $obj;
		my $s = eval "sub {$_}";
		if(!$@) {
			&$s(@args);
			next if(!$@);
		} else {
			print "ERROR: '$@' (this will be a dialog later)\n";
			$obj->xgtk_destroy();
			return undef;
		}
	}
	@{$obj->{__destr}} = map {
		my $this = $obj;
		my $s = eval "sub {$_}";
		if($@) {die("ARGH! creating destructor: '$@'")}
		$s;
	} @{$obj->{__destr}};
	return $obj;
}

sub get_sub {
	my($this) = $_[1];
	my $widget = $_[3];
	print "EVAL: $this $widget CODE: $_[2]\n";
	my $s = eval "sub {$_[2]}";
	if($@) { die("Error creating sub '$_[2]': '$@'") }
	return $s;
}

sub standard_attrs {
	my($this, $obj, $w, $a, $signal) = @_;
	if(defined $a->{usize}) {
		$w->set_usize(split ',',$a->{usize});
	}
	if(defined $a->{sensitive}) {
		$w->set_sensitive($a->{sensitive});
	}
	if(defined $signal and defined $a->{method}) {
		for my $n (split ',',$a->{method}) {
			$w->signal_connect($signal,
				sub {
					$obj->$n(@_);
				}
			);
		}
	}
}

sub table_attach_options {
	my($a) = @_;
	my @a = ([expand,fill],[expand,fill],0,0);
	if($a->{x_opts}) { $a[0] = [split ',',$a->{x_opts}] }
	if($a->{y_opts}) { $a[1] = [split ',',$a->{y_opts}] }
	return @a;
}

sub create_widget {
	my($this, $obj, $grovelem, $parent, $pack) = @_;
	print "CRR: '$grovelem'\n";
	my $n = $grovelem->name;
	my $a = $grovelem->attributes;
	my $c = $grovelem->contents;

	print "CR: $n\n";

	my($w,$npack);

	return if $n =~ /^help/; # These names for help file extraction only

	if($n eq "script") {
		return if $this->{InheritMethods}; # XXX ???
		my $scr = join '',@{$c}; # Assume no refs XXX
		my $sub = $this->get_sub($obj, $scr, $parent);
		my $sig = $a->{signal};
		if(!defined $sig) {
			$sig = $obj->{__default_signal}[-1];
			if(!defined $sig) {
				die("signal not given to script, no default");
			}
		}
		$parent->signal_connect($sig, $sub);
		return;
	} elsif($n eq "tooltip") {
		my $text = join '',@{$c};
		$tooltips->set_tip($parent, $text);
	} elsif($n eq "inherit") {
		if($a->{method}) {
			for my $meth (split ',',$a->{method}) {
				next if $this->{InheritMethods} and
					!grep {$_ eq $meth}
					  @{$this->{InheritMethods}};
				
			}
		}
		if($a->{widget}) {
			die("Widget inheritance not implemented yet");
		}
		return;
	} elsif($n eq "method") {
		return if $this->{InheritMethods} and
			!grep {$a->{name} eq $_} @{$this->{InheritMethods}};
		my $scr = join '',@{$c}; # Assume no refs XXX
		my $sub = $this->get_sub($obj,$scr);
		$obj->{__meth}{$a->{name}} = $sub;
		return;
	} elsif($this->{InheritMethods}) {
		for(@$c) {
			next if !ref $_;
			$this->create_widget($obj, $_, $parent, $pack);
		}
		return;
	} elsif($n eq "constructor") {
		push @{$obj->{__constr}}, join '', @{$c};
	} elsif($n eq "destructor") {
		push @{$obj->{__destr}}, join '', @{$c};
	} elsif($n eq "pack") {
		my $exp = $a->{expand};
		my $fill = $a->{fill};
		my $pad = $a->{padding};
		for(@$c) {
			next unless ref $_;
			$this->create_widget($obj, $_, $parent, 
			sub {
				$parent->pack_start($_[0], $exp, $fill, $pad);
			});
		}
		return;
	} elsif($n eq "tableCell") {
		for(@$c) {
			next unless ref $_;
			$this->create_widget($obj, $_, $parent,
			sub {
				$parent->attach($_[0],
					$a->{x0}, $a->{x1},
					$a->{y0}, $a->{y1},
					table_attach_options($a)
				);
			});
		}
	} elsif($n eq "tableRow") {
		my $ind = 0;
		for(@$c) {
			next unless ref $_;
			$this->create_widget($obj, $_, $parent,
			sub {
				$parent->attach($_[0],
					$ind, $ind+1,
					$a->{y0}, $a->{y1},
					table_attach_options($a)
				);
				$ind ++;
			});
		}
	} elsif($n eq "tableColumn") {
		my $ind = 0;
		for(@$c) {
			next unless ref $_;
			$this->create_widget($obj, $_, $parent,
			sub {
				$parent->attach($_[0],
					$a->{x0}, $a->{x1},
					$ind, $ind+1,
					table_attach_options($a)
				);
				$ind ++;
			});
		}
	} elsif($n eq "timeout") {
		my $scr = join '',@{$c}; # Assume no refs XXX
		my $sub = $this->get_sub($obj, $scr);
		print "ADD TO $scr: $sub\n";
		Gtk->timeout_add($a->{interval}, $sub);
	} elsif($n eq "CList" or $n eq "CTree") {
		# get columns
		my @col;
		for(@$c) {
			next unless ref $_;
			if($_->name  eq "column") {
				my $a = $_->{attributes};
				push @col, [$a->{name}, $a->{width}];
			}
		}
		print "CREATING: $n (@col) ",(map {$_->[0]} @col),"\n";
		if($n eq "CList") {
			$w = "Gtk::CList"->new_with_titles(
				map {$_->[0]} @col
			);
		} else {
			$w = "Gtk::CTree"->new_with_titles(
				0,
				map {$_->[0]} @col
			);
			# $w->set_column_auto_resize(0,1);
		}
		print "GOT: '$w'\n";
		$this->standard_attrs($obj, $w,$a);
		&{$pack}($w);
		for(0..$#col) {
			$w->set_column_width($_, $col[$_][1]);
		}
		$obj->{$a->{id}} = $w if(defined $a->{id});
		# Go through for scripts &c.
		for(@$c) {
			next unless ref $_;
			next if $_->name eq "column";
			$this->create_widget(
				$obj, $_,
				$w, sub {
					die("Can't pack into CList or CTree");
				}
			);
		}
		$w->show;
		# no return!
	} elsif($n eq "GnomeDialog") {
		my @buttons;
		for(@$c) {
			next unless ref $_;
			if($_->name eq "dialogButton") {
				push @buttons, $_;
			}
		}
		$w = Gnome::Dialog->new( $a->{title},
					 map {$_->{attributes}{name}} @buttons );
		my $b = $w->vbox;

		my @subs = map {
			$this->get_sub($obj, join '',@{$_->{contents}});
		} @buttons;

		$w->signal_connect("clicked",
			sub {
				my($w, $n) = @_;
				print "Got button '$n' ('$w')\n";
				if(defined $buttons[$n]->{attributes}{method}) {
					for my $n (split ',',
						$buttons[$n]->{attributes}{method}) {
						$obj->$n();
					}
				}
				&{$subs[$n]}();
			}
		);

		for(@$c) {
			next unless ref $_;
			next if $_->name eq "dialogButton";
			$this->create_widget(
				$obj, $_,
				$b, sub {
					$b->add($_[0]);
				}
			);
		}

		$w->show;

	} elsif($n eq "Adjustment") {
		# Only set in parent if we are really inside a Range
		# widget
		if($parent->isa(Gtk::Range)) {
			my $adj = Gtk::Adjustment->new(
				$a->{value},
				$a->{lower},
				$a->{upper},
				$a->{step_increment},
				$a->{page_increment},
				$a->{page_size}
			);
			$parent->set_adjustment($adj);
		}
	} elsif($widgets{$n}) {
		my $signal;
		($w,$npack,$signal) = &{$widgets{$n}}($a, $obj);
		if(!$w) {return}

	widget_normal:
		$obj->{$a->{id}} = $w if(defined $a->{id});
		$this->standard_attrs($obj, $w,$a, $signal);
		&{$pack}($w);

		push @{$obj->{__default_signal}}, $signal;

		for(@$c) {
			next unless ref $_;
			$this->create_widget(
				$obj, $_,
				$w, $npack || sub {
					$w->add($_[0]);
				}
			);
		}

		pop @{$obj->{__default_signal}};

		$w->show;
		# no return!
	} else {
		die("No element '$n' understood");
	}
	return $w;
}

# The actual widgets

%widgets = (
	Button => sub {
		my($a) = @_;
		my $l = $a->{text} || $a->{label};
		my $w;
		if(defined $l) {
			$w = Gtk::Button->new_with_label($l);
		} else {
			$w = Gtk::Button->new();
		}
		if($a->{pixmap}) {
			print "LOADING PIXMAP $a->{pixmap}\n";
			my $p = Gnome::Pixmap->new_from_file(
				my $pn = Gnome::Pixmap::file($a->{pixmap})
			);
			print "NAME WAS: $pn ($p)\n";
			$w->add($p);
			$p->show;
		}
		return ($w, sub {print "ADD INSIDE BUT: $_[0]\n";
				$w->add($_[0])}, "clicked");
	},
	ToggleButton => sub {
		my($a) = @_;
		my $l = $a->{text} || $a->{label};
		my $w;
		if(defined $l) {
			$w = Gtk::ToggleButton->new_with_label($l);
		} else {
			$w = Gtk::ToggleButton->new();
		}
		if($a->{pixmap}) {
			print "LOADING PIXMAP $a->{pixmap}\n";
			my $p = Gnome::Pixmap->new_from_file(
				my $pn = Gnome::Pixmap::file($a->{pixmap})
			);
			print "NAME WAS: $pn ($p)\n";
			$w->add($p);
			$p->show;
		}
		return ($w, sub {print "ADD INSIDE BUT: $_[0]\n";
				$w->add($_[0])}, "toggled");
	},
	GnomeStockButton => sub {
		my($a) = @_;
		return (Gnome::stock_button($a->{type}), undef, "clicked");
	},
	RadioButton => sub {
		my($a, $obj) = @_;
		my $l = $a->{text} || $a->{label};
		my $prev = $a->{group};
		my $current;
		if(defined $prev) {
			my $p = $obj->{$prev} or die("RadioButton group '$prev' not found");
			$current = Gtk::RadioButton->new($l, $p);
		} else {
			$current = Gtk::RadioButton->new($l);
		}
		return $current;
	},
	Label => sub {
		my($a) = @_;
		my $l = $a->{text} || $a->{label};
		my $lab = Gtk::Label->new($l);
		if($a->{justify}) {
			print "JUST: $a->{justify}\n";
			$lab->set_justify($a->{justify});
		}
		return $lab;
	},
	(
		map {
			my $name = $_;
			($_ => sub {
				my ($a) = @_;
				my $w = "Gtk::$name"->new(
					Gtk::Adjustment->new(0,0,1,1,1,1)
				);
				$w->set_draw_value($a->{draw_value});
				$w->set_digits($a->{digits})
					if defined $a->{digits};
				return $w;
			})
		} qw/HScale VScale/
	),
	GnomePixmap => sub {
		my($a) = @_;
		my $f;
		if(defined $a->{file}) {
			$f = Gnome::Pixmap::file($a->{file});
		} elsif(defined $a->{absfile}) {
			$f = $a->{absfile}
		} else {
			die("Pixmap name not given");
		}
		return Gnome::Pixmap->new_from_file($f);
	},
	MenuItem => sub {
		my($a,$obj) = @_;
		my $l = $a->{text} || $a->{label} ||
			die ("No menuitem label or text") ;
		my $w;
		if(defined $a->{stock}) {
			print "Stock: $a->{stock}, $l\n";
			$w = Gnome::stock_menu_item($a->{stock}, $l);
		} else {
			$w = Gtk::MenuItem->new($l);
		}
		return ($w, sub {
			print "Add MENUI: $_[0]\n";
			if((ref $_[0]) =~ /Menu/) {
				$w->set_submenu($_[0]);
			} else {
				$w->add($_[0]);
			}
		},
		"activate");
	},
	TopLevel => sub {
		my($a) = @_;
		return Gtk::Window->new('toplevel');
	},
	Frame => sub {
		my($a) = @_;
		return Gtk::Frame->new($a->{label});
	},
	GnomeCanvas => sub {
		my($a) = @_;
		my $c = Gnome::Canvas->new();
		$c->set_scroll_region(split ',',$a->{scroll_region}) 
		 if $a->{scroll_region};
		return $c;
	},
	GnomeAACanvas => sub {
		my($a) = @_;
		my $c = Gnome::Canvas->new_aa();
		return $c;
	},
	Table => sub {
		my($a) = @_;
		return (Gtk::Table->new($a->{x}, $a->{y}, $a->{homog}),
		 sub {
		 	die("Can't put directly in a Table, use tableCell, tableRow or tableColumn");
		 }
		);
	},
	VBox => sub {
		my($a) = @_;
		return Gtk::VBox->new($a->{homog},$a->{spacing});
	},
	HBox => sub {
		my($a) = @_;
		return Gtk::HBox->new($a->{homog},$a->{spacing});
	},
	ScrolledWindow => sub {
		my($a) = @_;
		return Gtk::ScrolledWindow->new(undef,undef);
	},
	Entry => sub {
		return (Gtk::Entry->new(), undef, 'activate');
	},
	Text => sub {
		my($a) = @_;
		my $t = Gtk::Text->new();
		if(defined $a->{editable}) {
			$t->set_editable($a->{editable});
		}
		return $t;
	},
	MenuBar => sub {
		my $m = Gtk::MenuBar->new();
		return ($m, sub {$m->append($_[0])});
	},
	Menu => sub {
		my $m = Gtk::Menu->new();
		return ($m, sub {$m->append($_[0])});
	},
	GnomeAbout => sub {
		my($a) = @_;
		my $m = Gnome::About->new(
			$a->{title},
			$a->{version},
			$a->{copyright},
			$a->{authors},
			$a->{comments}
		);
	},
);


package XGtk::Widget;


sub AUTOLOAD {
	my $this = shift;
	my $n = $AUTOLOAD;
	$n =~ s/.*:://;
	if($this->{__meth}{$n}) {
		# print "CALL METHOD $n (@_)\n";
		$this->{__meth}{$n}->(@_);
	} else {
		print "Available: ",(join ' ',keys %{$this->{__meth}}),"\n";
		die("No method '$n' found");
	}
}

sub xgtk_destroy {
	print "XGTK_DESTROY: '$_[0]' '$_[0]{__destr}'\n";
	for(@{$_[0]{__destr}}) {&{$_}()}
	for(@{$_[0]{__widg}}) {
		print "D $_\n";
		$_->destroy if ref $_;
	}
	%{$_[0]} = ();
}

sub DESTROY {
	$_[0]->xgtk_destroy;
}
use strict;


# a package that publishes the DTD limitations by XGtk
# INCOMPLETE

package XGtk::DTD;
use vars qw/@widgets @routines @scripts %nonpackable %signalable %elements
	%attributes/;

@widgets = qw/HBox VBox Button CTree ScrolledWindow/;
@routines = qw/constructor destructor method/;
@scripts = qw/script/;

%nonpackable = map {($_=>1)} qw/CTree/;
%signalable = map {($_=>1)} qw/Button/;

sub element {
	return {Contents => $_[0], Attrs => $_[1]}
}

%elements = (
XGtk => element([qw/CLASS TopLevel/],
		[]),
TopLevel => element([@widgets, @routines],
			[]),
(map {
  ($_ => element([@widgets, @routines, ($signalable{$_}?@scripts:())]))
} grep {!$nonpackable{$_}} @widgets),

method => element([],[]),
constructor => element([],[]),
destructor => element([],[]),
);

use vars qw/$meth $text $label $usize $sens $id @bool $stock/;

@bool=("CHOICE", [0,1]);

$meth=["method", "STRING"];
$text=["text", "STRING"];
$label=["label", "STRING"];
$usize=["usize", "STRING"];
$sens=["sensitive", "CHOICE", [0,1]];
$id=["id","STRING"];
$stock=["stock", "CHOICE", [qw/OK CANCEL APPLY/]];


%attributes = (
	Button => [$label, $text, $id, $meth, $usize, $sens],
	VBox => [$id, $usize],
	HBox => [$id, $usize],
	Table => [["x", "STRING"],["y","STRING"], $id, [homog => @bool]],
	ScrolledWindow => [$id, $usize],
	Entry => [$id, $usize],
	Text => [$id, $usize],
	MenuBar => [$id, $usize],
	Menu => [$id, $usize],
	GnomeAbout => [map {[$_,"STRING"]} qw/title version copyright
				authors comments/],
	MenuItem => [$label, $text, $stock, $id],
	TopLevel => [$id, $usize],
	Frame => [$id, $usize],

	pack => [[expand => "STRING"], [homog => @bool]],
	script => [["signal" => "STRING"]],
	method => [["name" => "STRING"]],
	constructor => [],
	destructor => [],

	tableCell => [map {[$_,"STRING"]} qw/x0 x1 y0 y1/],
	tableRow => [map {[$_,"STRING"]} qw/y0 y1/],
	tableColumn => [map {[$_,"STRING"]} qw/x0 x1/],

	column => [["name", "STRING"]],

	timeout => [["interval","STRING"]],
	CList => [$id, $usize],
	CTree => [$id, $usize],
	GnomeDialog => [["title", "STRING"]],
	dialogButton => [["name","STRING"],["method", "STRING"]],

);

sub new {
	my($type) = @_;
	bless {}, $type;
}

sub possible_contained {
	my($this, $element) = @_;
	return $elements{$element}{Contents};
}

sub elements {
	my($this) = @_;
	return [sort keys %elements];
}

sub attributes {
	my($this, $element) = @_;
	return $attributes{$element};
}

	###
	### Code borrowed from Ken MacLeod's IDs.pm module
	### This will eventually be part of XML:::Grove
	###

	package XML::Grove::IDs;

	use XML::Grove::Visitor;

	sub new {
	    my ($type, $name, $elements) = @_;
	    $name = 'id' if(!defined $name);
	    return (bless {Name => $name, Elements => $elements}, $type);
	}

	sub visit_grove {
	    my $self = shift; my $grove = shift; my $hash = shift;
	    $grove->children_accept ($self, $hash);
	}

	sub visit_element {
	    my $self = shift; my $element = shift; my $hash = shift;

	    if(!$self->{Elements} or $self->{Elements}{$element->{name}}) {
		    my $id = $element->attr($self->{Name});
		    $hash->{$id} = $element
			if (defined $id);
	    }

	    $element->children_accept ($self, $hash);
	}

	sub visit_entity {
	}

	sub visit_pi {
	}

	sub visit_comment {
	}

	sub visit_scalar {
	}

	###
	### Extend the XML::Grove and XML::Grove::Element packages with our
	### new function.
	###

	package XML::Grove;

	sub get_ids {
	    my $self = shift;

	    my $hash = {};
	    $self->accept(XML::Grove::IDs->new(@_), $hash);
	    return $hash;
	}

	package XML::Grove::Element;

	sub get_ids {
	    my $self = shift;

	    my $hash = {};
	    $self->accept(XML::Grove::IDs->new(@_), $hash);
	    return $hash;
	}

	package XML::Grove::Iter;

	sub get_ids {
	    my $self = shift;

	    my $hash = {};
	    $self->accept(XML::Grove::IDs->new(@_), $hash);
	    return $hash;
	}

	package XML::Grove::Element::Iter;

	sub get_ids {
	    my $self = shift;

	    my $hash = {};
	    $self->accept(XML::Grove::IDs->new(@_), $hash);
	    return $hash;
	}

	1;
