# A quick&dirty help extractor.

$progname = $ARGV[0];

my $str = join '',<>; # Get all the text from the arguments.

my $res;
while($str =~ m|<help>(.*?)</help>|gs) {
	$res .= $1;
}

print qq{
<HTML>
<HEAD>
<TITLE>Documentation for $progname</TITLE>
<BODY>
$res
</BODY>
};
